document.addEventListener("DOMContentLoaded", function(event) {
  // Cria Table of Contents automaticamente
  let elements = [];
  let menu = '<ul>';

  Array.prototype.forEach.call(document.querySelectorAll('section'), function(el, i){
    let subElements = [];

    el.querySelectorAll('h3').forEach(function(item, i){
      let subElement = {
        title: item.innerHTML,
        id: item.id
      }
      subElements.push(subElement);
    });

    let element = {
      title: el.querySelectorAll('h2')[0].innerHTML,
      id: el.id,
      subItem: subElements
    }

    elements.push(element);
  });

  elements.forEach(function(elem){
    menu = menu + '<li><a href="#' + elem.id + '" id="' + elem.id + '_link">' + elem.title + '</a></li>';
    if(elem.subItem.length > 0){
      menu = menu + '<ul>';
      elem.subItem.forEach(function(subEl){
        menu = menu + '<li><a href="#' + subEl.id + '" id="' + subEl.id + '_link">' + subEl.title + '</a></li>';
      });
      menu = menu + '</ul>';
    }
  });

  menu = menu + '</ul>';
  document.querySelectorAll('nav')[0].innerHTML = menu;

// funções de scroll

  window.onscroll = function (ev) {
    let headerSize = document.querySelectorAll('header')[0].offsetHeight;
    var nVScroll = document.documentElement.scrollTop || document.body.scrollTop;
    if (nVScroll >= headerSize){   //Trava menu lateral dentro da div.
      document.querySelectorAll('nav')[0].style.position = 'fixed';
      document.querySelectorAll('nav')[0].style.top = '0';
    } else {
      document.querySelectorAll('nav')[0].style.position = 'absolute';
      document.querySelectorAll('nav')[0].style.top = '100vh';
    }

    Array.prototype.forEach.call(document.querySelectorAll('section'), function(el, i){
      if(nVScroll >= el.offsetTop - (headerSize*2 / 3) && nVScroll <= el.offsetTop + el.offsetHeight - (headerSize*2 / 3)) {
        document.getElementById(el.id + '_link').style.textDecoration = "underline";
        document.getElementById(el.id + '_link').style.fontWeight = "400";
      } else {
        document.getElementById(el.id + '_link').style.textDecoration = "none";
        document.getElementById(el.id + '_link').style.fontWeight = "300";
      }
    });

  }


});
